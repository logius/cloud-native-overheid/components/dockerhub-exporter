# Dockerhub Exporter

Metrics exporter to be used as a Prometheus data source.
These metrics show the number of available anonymous pulls left in the Dockerhub rate limit.

Based on examples in [gitlab.com](https://gitlab.com/gitlab-com/marketing/corporate_marketing/developer-evangelism/code/docker-hub-limit-exporter)


## Component
The component consists of:
- A deployment manifest that deploys an exporter pod. Prometheus can scrape the pod on tcp port 8881.

## Requirements
- Docker image based on [this project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/developer-evangelism/code/docker-hub-limit-exporter)
- The following minimal set of parameters

```
config:
  dockerhub_exporter:
    image_repo: <Container image name without tag>
    image_tag: <Container image tag>
```

- The servicemonitor can be deployed to a different namespace to support Prometheus deployments that do not watch the exporter namespace. Use `config.dockerhub_exporter.servicemonitor_namespace` (default: same as `config.dockerhub_exporter.namespace`).

## Post Actions
Optionally, import metrics into Grafana. A simplified version of the dashboard from the examples in [gitlab.com](https://gitlab.com/gitlab-com/marketing/corporate_marketing/developer-evangelism/code/docker-hub-limit-exporter) can be found in `files/example-grafana-dashboard.json`. Add your Prometheus datasource instead of `My-Prometheus-Datasource`.

![Grafana Dashboard](dockerhub-exporter-grafana.png)
